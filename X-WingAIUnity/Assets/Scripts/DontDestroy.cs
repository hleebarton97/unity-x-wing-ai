﻿using UnityEngine;

/// ////////////////////////////////////////////////////////
// D O N T   D E S T R O Y
/// ////////////////////////////////////////////////////////

public class DontDestroy : MonoBehaviour {
    
    // Don't destroy current object on load
    void Awake() {
        DontDestroyOnLoad(this.gameObject);
    }
}
