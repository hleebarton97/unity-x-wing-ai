﻿using UnityEngine;

public class Outline : MonoBehaviour {

    /// ////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    #pragma warning disable 0649
    [SerializeField] private Material outlineMaterial;
    [SerializeField] private float outlineScaleFactor;
    [SerializeField] private Color outlineColor; 
    private Renderer outlineRenderer;

    /// ////////////////////////////////////////////////////////
    // O U T L I N E
    /// ////////////////////////////////////////////////////////

    private void Start() {
        this.outlineRenderer = CreateOutline(this.outlineMaterial, this.outlineScaleFactor, this.outlineColor);
    }

    private void OnMouseOver () {
        transform.Rotate(Vector3.up, 1f, Space.World);
    }

    public void ShowOutline() {
        this.outlineRenderer.enabled = true;
    }

    public void HideOutline() {
        this.outlineRenderer.enabled = false;
    }

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ////////////////////////////////////////////////////////

    private Renderer CreateOutline (Material material, float scaleFactor, Color color) {
        // Create copy of current object
        GameObject objectToOutline = Instantiate(gameObject, transform.position, transform.rotation, transform);
        
        // Get renderer and setup material
        Renderer renderer = objectToOutline.GetComponent<Renderer>();
        renderer.material = material;
        renderer.material.SetColor("_OutlineColor", color);
        renderer.material.SetFloat("_ScaleFactor", scaleFactor);
        renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        
        // Disable components
        objectToOutline.GetComponent<Outline>().enabled = false;
        if (objectToOutline.GetComponent<Collider>() != null) {
            objectToOutline.GetComponent<Collider>().enabled = false;
        }
        renderer.enabled = false;

        return renderer;
    }
}
