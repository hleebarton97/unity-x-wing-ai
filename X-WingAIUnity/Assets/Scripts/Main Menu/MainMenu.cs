﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// ////////////////////////////////////////////////////////
// M A I N   M E N U
/// ////////////////////////////////////////////////////////

public class MainMenu : MonoBehaviour
{
    // Begin using the AI.
    public void StartAI (string aiType) {
        // Set selected AI type
        PlayerPrefs.SetString(SaveKeys.SCENE_TO_LOAD, aiType);
        // Load select ship scene
        SceneManager.LoadScene(SceneNames.AI_SHIP_SELECTION);
    }

    // Exit application.
    public void Quit() {
        Debug.Log("Closing application...");
        Application.Quit();
    }
}
