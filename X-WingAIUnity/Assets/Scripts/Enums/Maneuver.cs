﻿/// ////////////////////////////////////////////////////////
// M A N E U V E R
/// ////////////////////////////////////////////////////////
public enum Maneuver {
    FORWARD,
    BANK_LEFT,
    BANK_RIGHT,
    TURN_LEFT,
    TURN_RIGHT,
    K_TURN,
    S_LOOP_LEFT,
    S_LOOP_RIGHT,
    TALLON_ROLL,LEFT,
    TALLON_ROLL_RIGHT,
    REVERSE,
    REVERSE_BANK_LEFT,
    REVERSE_BANK_RIGHT,
    STATIONARY
}
