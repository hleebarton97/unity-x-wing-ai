﻿/// ////////////////////////////////////////////////////////
// A C T I O N
/// ////////////////////////////////////////////////////////
public enum Action {
    BARREL_ROLL,
    BARREL_ROLL_RED,
    BOOST,
    BOOST_RED,
    CALCULATE,
    CLOAK,
    COORDINATE,
    COORDINATE_RED,
    EVADE,
    EVADE_RED,
    EVADE_FORCE,
    FOCUS,
    JAM,
    JAM_RED,
    RELOAD,
    RELOAD_RED,
    REINFORCE,
    REINFORCE_RED,
    ROTATE_ARC,
    ROTATE_ARC_RED,
    SLAM,
    TARGET_LOCK,
    TARGET_LOCK_RED,
}
