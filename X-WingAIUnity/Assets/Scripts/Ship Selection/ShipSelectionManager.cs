﻿using System;
using UnityEngine;

/// ////////////////////////////////////////////////////////
// S H I P   A T T R I B U T E   S E L E C T I O N   M E N U
/// ////////////////////////////////////////////////////////

public class ShipSelectionManager : MonoBehaviour {

    /// ////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    [SerializeField] private Faction selectedFaction;
    [SerializeField] private bool hasSelectedFaction = false;
    [SerializeField] private ShipType selectedShipType;
    [SerializeField] private Ship selectedShip;
    [SerializeField] private bool hasSelectedShip = false;

    [SerializeField] private bool isManagingShips = false;

    [SerializeField] private int selectedShipPilotSkill = 0;
    [SerializeField] private int selectedShipShield = 0;
    [SerializeField] private int selectedShipHull = 0;

    /// ////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    // Selected ships container
    [Header("Selected Ships Container")]
    public GameObject selectedShipsContainer;

    [Header("Selected Ships Management")]
    public GameObject elementPrefab;
    public Transform elementParent;

    // Prefab ships to instantiate
    // Rebel
    // Empire
    [Header("Empire Prefabs")]
    public GameObject shipEmpireDecimatorPrefab;
    // Scum
    [Header("Scum Prefabs")]
    public GameObject shipScumZ95Prefab;
    // Resistance
    // First Order
    // Republic
    // Seperatist

    // Ship selection containers
    [Header("Ship Faction Containers")]
    public GameObject empireShipContainer;
    public GameObject scumShipContainer;

    /// ////////////////////////////////////////////////////////
    // S E L E C T I O N   M A N A G E R
    /// ////////////////////////////////////////////////////////

    // User has selected a faction
    public void SelectFaction (int faction) {
        this.SetSelectedFaction((Faction)faction);
        this.ShowFactionSelectableShips();
        this.SetHasSelectedFaction(true);
    }

    // User selected a ship.
    public void SelectShip (int shipType) {
        this.SetSelectedShipType((ShipType)shipType);
        this.HideAllSelectableShips();
        this.SetHasSelectedShip(true);
    }

    // User finished updated ship attributes.
    public void ConfirmShipSelection (int pilotSkill, int shield, int hull) {
        this.selectedShipPilotSkill = pilotSkill;
        this.selectedShipShield = shield;
        this.selectedShipHull = hull;
        this.CheckAndCreateSelectedShip();
        this.SetHasSelectedFaction(false);
        this.SetHasSelectedShip(false);
    }

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ////////////////////////////////////////////////////////

    // Show the ships that can be selected by faction.
    private void ShowFactionSelectableShips () {
        if (this.selectedFaction == Faction.EMPIRE) {
            this.empireShipContainer.SetActive(true);
        } else if (this.selectedFaction == Faction.SCUM) {
            this.scumShipContainer.SetActive(true);
        }
    }

    // Hide all selectable ships.
    private void HideAllSelectableShips () {
        empireShipContainer.SetActive(false);
        scumShipContainer.SetActive(false);
    }

    // Instantiate the selected ship as a child of the ships
    // object.
    private void CheckAndCreateSelectedShip () {
        switch (this.selectedShipType) {
            case ShipType.VT_49_DECIMATOR: {
                this.CreateShip(shipEmpireDecimatorPrefab);
                break;
            }
            case ShipType.Z95_AF4_HEADHUNTER: {
                this.CreateShip(shipScumZ95Prefab);
                break;
            }
        }
    }

    // Instantiate the specified ship as a selected ship for the
    // next scene.
    private void CreateShip (GameObject ship) {
        // Add ship to selected ships container
        GameObject createdShip = Instantiate(ship, this.selectedShipsContainer.transform);
        this.selectedShip = createdShip.GetComponent<Ship>();
        this.SetSelectedShipAttributes(this.selectedShipPilotSkill, this.selectedShipShield, this.selectedShipHull);
        createdShip.SetActive(false);
        // Add ship management element to scroll view content
        GameObject newElement = Instantiate(this.elementPrefab, this.elementParent);
        Element element = newElement.GetComponent<Element>();
        Action<int> onClickEvent = DeleteSelectedShip;
        element.Setup(
            this.elementParent,
            (this.selectedShipsContainer.transform.childCount - 1),
            this.selectedShip.title,
            this.selectedShip.GetPilotSkill().ToString(),
            onClickEvent
        );
    }

    private void SetSelectedShipAttributes (int pilotSkill, int shield, int hull) {
        this.selectedShip.SetAllAttributes(pilotSkill, shield, hull);
    }

    // Delete a selected ship.
    private void DeleteSelectedShip (int index) {
        // Destroy GameObjects
        GameObject elementAtIndex = this.elementParent.GetChild(index).gameObject;
        GameObject shipAtIndex = this.selectedShipsContainer.transform.GetChild(index).gameObject;
        // Remove parent & destroy
        elementAtIndex.transform.SetParent(null);
        shipAtIndex.transform.SetParent(null);
        Destroy(elementAtIndex);
        Destroy(shipAtIndex);
        // Update index
        for (int i = 0; i < this.elementParent.childCount; i++) {
            Element element = this.elementParent.GetChild(i).GetComponent<Element>();
            if (element.GetCurrentIndex() != i) {
                element.SetCurrentIndex(i);
            }
        }
    }

    /// ////////////////////////////////////////////////////////
    // S E T T E R S
    /// ////////////////////////////////////////////////////////

    // Set the user selected faction.
    public void SetSelectedFaction (Faction selectedFaction) {
        this.selectedFaction = selectedFaction;
    }

    // Set the user selected ship type.
    public void SetSelectedShipType (ShipType selectedShipType) {
        this.selectedShipType = selectedShipType;
    }

    // Set that the user has selected a faction.
    public void SetHasSelectedFaction (bool hasSelectedFaction) {
        if (!hasSelectedFaction) {
            this.HideAllSelectableShips();
        }
        this.hasSelectedFaction = hasSelectedFaction;
    }

    // Set that the user has selected a ship.
    public void SetHasSelectedShip (bool hasSelectedShip) {
        this.hasSelectedShip = hasSelectedShip;
    }

    // Set that the user is managing selected ships.
    public void SetIsManagingShips (bool isManagingShips) {
        this.isManagingShips = isManagingShips;
    }

    /// ////////////////////////////////////////////////////////
    // G E T T E R S
    /// ////////////////////////////////////////////////////////
    
    // Has the user selected a faction?
    public bool GetHasSelectedFaction () {
        return this.hasSelectedFaction;
    }

    // Has the user selected a ship?
    public bool GetHasSelectedShip () {
        return this.hasSelectedShip;
    }

    // Get how many ships the user has selected.
    public int GetSelectedShipsCount () {
        return this.selectedShipsContainer.transform.childCount;
    }

    public bool GetIsManagingShips () {
        return this.isManagingShips;
    }
}
