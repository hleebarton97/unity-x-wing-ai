﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// ////////////////////////////////////////////////////////
// M A N A G E   S H I P   E L E M E N T
/// ////////////////////////////////////////////////////////

public class Element : MonoBehaviour {

    /// ////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /// ////////////////////////////////////////////////////////
    
    [SerializeField] private int index;

    #pragma warning disable 0649
    [SerializeField] private TextMeshProUGUI shipTitleText;
    [SerializeField] private TextMeshProUGUI shipSkillText;
    [SerializeField] private Button deleteButton;

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ////////////////////////////////////////////////////////
    
    public void Setup (Transform parent, int index, string shipTitle, string shipSkill, Action<int> onClick) {
        this.SetCurrentIndex(index);
        this.SetElementParent(parent);
        this.SetAnchorsAndPivot();
        this.SetShipTitleText(shipTitle);
        this.SetShipSkillText(shipSkill);
        this.SetDeleteButtonOnClickEvent(onClick);
    }

    /// ////////////////////////////////////////////////////////
    // G E T T E R S
    /// ////////////////////////////////////////////////////////

    public int GetCurrentIndex () {
        return this.index;
    }

    public string GetShipTitleText () {
        return this.shipTitleText.text;
    }

    /// ////////////////////////////////////////////////////////
    // S E T T E R S
    /// ////////////////////////////////////////////////////////

    // Set current element index in list
    public void SetCurrentIndex (int index) {
        this.index = index;
    }

    // Set anchor preset
    public void SetAnchorsAndPivot () {
        RectTransform rectTransform = this.gameObject.GetComponent<RectTransform>();
        rectTransform.anchorMax = new Vector2(1f, 1f);
        rectTransform.anchorMin = new Vector2(0f, 1f);
        rectTransform.pivot = new Vector2(0.5f, 1f);
    }

    // Set the parent of the element
    public void SetElementParent (Transform content) {
        this.gameObject.transform.SetParent(content);
        RectTransform rectTransform = this.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(0f, (this.index * rectTransform.rect.height * -1f));
    }

    // Set the ship title text label.
    public void SetShipTitleText (string text) {
        this.shipTitleText.text = text;
    }

    // Set the ship's pilot skill level text label.
    public void SetShipSkillText (string text) {
        this.shipSkillText.text = text;
    }

    // Set the onClick even of the event button
    public void SetDeleteButtonOnClickEvent (Action<int> onClick) {
        this.deleteButton.onClick.AddListener(
            delegate {
                onClick(this.index);
            }
        );
    }
}
