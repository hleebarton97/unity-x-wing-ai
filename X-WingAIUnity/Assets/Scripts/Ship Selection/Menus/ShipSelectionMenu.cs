﻿/// ////////////////////////////////////////////////////////
// S H I P   S E L E C T I O N   M E N U
/// ////////////////////////////////////////////////////////

public class ShipSelectionMenu : SelectionMenu {

    /// ////////////////////////////////////////////////////////
    // U I   H A N D L E R
    /// ////////////////////////////////////////////////////////
    
    public void GoBack () {
        this.shipSelectionManager.SetHasSelectedFaction(false);
    }

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ////////////////////////////////////////////////////////

    // Determine if the ship selection menu should be displayed or not.
    protected override void ShowOrHideMenu () {
        if (this.shipSelectionManager.GetHasSelectedFaction() && !this.shipSelectionManager.GetHasSelectedShip()) {
            this.Show();
        }
        else {
            this.Hide();
        }
    }
}
