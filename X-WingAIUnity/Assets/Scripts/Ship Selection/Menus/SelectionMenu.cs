﻿using UnityEngine;

public abstract class SelectionMenu : MonoBehaviour {

    /// ////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    public GameObject menu;
    public ShipSelectionManager shipSelectionManager;

    /// ////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    protected bool prevStateOfHasSelectedFaction;
    protected bool prevStateOfHasSelectedShip;
    protected bool prevStateOfIsManagingShips;

    /// ////////////////////////////////////////////////////////
    // U I   H A N D L E R
    /// ////////////////////////////////////////////////////////

    // Start is called before the first frame update
    void Start () {
        this.prevStateOfHasSelectedFaction = this.shipSelectionManager.GetHasSelectedFaction();
        this.prevStateOfHasSelectedShip = this.shipSelectionManager.GetHasSelectedShip();
        this.prevStateOfIsManagingShips = this.shipSelectionManager.GetIsManagingShips();
    }

    // Update is called once per frame
    void Update () {
        // Determine if the user has selected a faction or gone back to faction menu
        this.ShouldShowOrHideMenu();
    }

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ////////////////////////////////////////////////////////

    // Determine if the ship selection menu should be displayed or not.
    protected void ShouldShowOrHideMenu () {
        if (TestShouldUpdate()) {
            // Set updated state of selected
            this.prevStateOfHasSelectedFaction = this.shipSelectionManager.GetHasSelectedFaction();
            this.prevStateOfHasSelectedShip = this.shipSelectionManager.GetHasSelectedShip();
            this.prevStateOfIsManagingShips = this.shipSelectionManager.GetIsManagingShips();
            // Hide or Show the faction menu
            this.ShowOrHideMenu();
        }
    }

    // Logic to show or hide menu
    protected abstract void ShowOrHideMenu ();

    // Set the faction selection menu to active.
    protected void Hide () {
        this.menu.SetActive(false);
    }

    // Set the faction selection menu to not active.
    protected void Show () {
        this.menu.SetActive(true);
    }

    // Test to see if function should run
    private bool TestShouldUpdate () {
        if (this.prevStateOfHasSelectedFaction != this.shipSelectionManager.GetHasSelectedFaction()
            || this.prevStateOfHasSelectedShip != this.shipSelectionManager.GetHasSelectedShip()
            || this.prevStateOfIsManagingShips != this.shipSelectionManager.GetIsManagingShips()) {
            return true;
        }
        return false;
    }
}
