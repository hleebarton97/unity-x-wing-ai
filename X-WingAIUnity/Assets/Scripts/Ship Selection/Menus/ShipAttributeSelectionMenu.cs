﻿using UnityEngine;
using TMPro;

/// ////////////////////////////////////////////////////////
// S H I P   A T T R I B U T E   S E L E C T I O N   M E N U
/// ////////////////////////////////////////////////////////

public class ShipAttributeSelectionMenu : SelectionMenu {

    /// ////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /// ////////////////////////////////////////////////////////
    
    [SerializeField] private int pilotSkill;
    [SerializeField] private int shield;
    [SerializeField] private int hull;

    /// ////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    public TextMeshProUGUI pilotSkillText;
    public TextMeshProUGUI shieldText;
    public TextMeshProUGUI hullText;

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ////////////////////////////////////////////////////////

    // Determine if the ship attribute selection menu should be displayed or not.
    protected override void ShowOrHideMenu () {
        if (this.shipSelectionManager.GetHasSelectedFaction() && this.shipSelectionManager.GetHasSelectedShip()) {
            this.Show();
        } else {
            this.Hide();
        }
    }

    private void UpdatePilotSkillText () {
        this.pilotSkillText.text = this.pilotSkill.ToString();
    }

    private void UpdateShieldText () {
        this.shieldText.text = this.shield.ToString();
    }

    private void UpdateHullText () {
        this.hullText.text = this.hull.ToString();
    }

    public void ConfirmShipSelection () {
        this.shipSelectionManager.ConfirmShipSelection(
            this.pilotSkill,
            this.shield,
            this.hull
        );
    }

    public void IncreasePilotSkill () {
        this.pilotSkill++;
        this.UpdatePilotSkillText();
    }

    public void DecreasePilotSkill () {
        if (this.pilotSkill != 0) {
            this.pilotSkill--;
            this.UpdatePilotSkillText();
        }
    }

    public void IncreaseShield () {
        this.shield++;
        this.UpdateShieldText();
    }

    public void DecreaseShield () {
        if (this.shield != 0) {
            this.shield--;
            this.UpdateShieldText();
        }
    }

    public void IncreaseHull () {
        this.hull++;
        this.UpdateHullText();
    }

    public void DecreaseHull () {
        if (this.hull != 0) {
            this.hull--;
            this.UpdateHullText();
        }
    }
}
