﻿using UnityEngine;

/// ////////////////////////////////////////////////////////
// S H I P
/// ////////////////////////////////////////////////////////

public class Ship : MonoBehaviour {

    /// ////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    public Faction faction;
    public ShipType shipType;
    public string title;

    [SerializeField] private int pilotSkill; // Determines order if > 1 AI ship

    [SerializeField] private int shield;
    [SerializeField] private int hull;

    /**
     * NOTE: Instead of a Tuple, each index references the
     * other's index as a pair.
     * 
     * Instead of (TARGET_LOCK, FREE)
     * It is: actions[0] = TARGET LOCK, actionTypes[0] = FREE
     * (Done so these may be selected in the inspector)
     */
    public Action[] actions;
    public ActionType[] actionTypes;

    /// ////////////////////////////////////////////////////////
    // P R O T E C T E D   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    protected Camera mainCamera;
    protected Canvas shipCanvas;
    
    protected (Maneuver Maneuver, int Distance, int Difficulty)[][] maneuvers;
    protected (Maneuver Maneuver, int Distance, int Difficulty)[][] maneuversWhenTargetIsClosing;
    protected (Maneuver Maneuver, int Distance, int Difficulty)[][] maneuversWhenTargetIsRetreating;
    protected (Maneuver Maneuver, int Distance, int Difficulty)[][] maneuversWhenTargetIsOutOfRange;
    protected (Maneuver Maneuver, int Distance, int Difficulty)[][] maneuversWhenStressed; 

    /// ////////////////////////////////////////////////////////
    // S H I P
    /// ////////////////////////////////////////////////////////

    protected virtual void Start () {
        // Set camera component for canvas
        this.SetCanvasCamera();
        // Verify what Scene we are in
        // this.ActivateComponentsBasedOnScene();
    }

    // Set the main camera of the ship's ui canvas.
    private void SetCanvasCamera () {
        shipCanvas = gameObject.GetComponentInChildren<Canvas>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        shipCanvas.worldCamera = mainCamera;
    }

    // Enable or Disable ship componenets based on current scene.
    /*protected void ActivateComponentsBasedOnScene () {
        // Get components
        TargetingSystem targetingSystem = gameObject.GetComponent<TargetingSystem>();
        GameObject canvas = shipCanvas.transform.gameObject;
        // Determine activity
        Debug.Log(SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name == SceneNames.AI_SHIP_SELECTION) {
            targetingSystem.enabled = false;
            canvas.SetActive(false);
        } else {
            if (!targetingSystem.enabled) {
                targetingSystem.enabled = true;
            }
            if (!canvas.activeInHierarchy) {
                canvas.SetActive(true);
            }
        }
    }*/

    /// ////////////////////////////////////////////////////////
    // S H I P   S E T T E R S
    /// ////////////////////////////////////////////////////////
    
    // Set the pilot level, shields, and hull health of the ship.
    public void SetAllAttributes (int pilotSkill, int shield, int hull) {
        this.SetPilotSkill(pilotSkill);
        this.SetShield(shield);
        this.SetHull(hull);
    }
    
    // Set pilot level.
    public void SetPilotSkill (int pilotSkill) {
        this.pilotSkill = pilotSkill;
    }

    // Set shield health
    public void SetShield (int shield) {
        this.shield = shield;
    }

    // Set hull health.
    public void SetHull (int hull) {
        this.hull = hull;
    }

    /// ////////////////////////////////////////////////////////
    // S H I P   G E T T E R S
    /// ////////////////////////////////////////////////////////
    
    // Get AI pilot skill level.
    public int GetPilotSkill () {
        return this.pilotSkill;
    }

    // Choose maneuver based on direction and activity of target.
    public (Maneuver Maneuver, int Distance, int Difficulty) GetShipManeuver (Direction direction, Activity activity) {
        switch (activity) {
            case Activity.TARGET_CLOSING: {
                return GetClosingManeuver(direction);
            }
            case Activity.TARGET_RETREATING: {
                return GetRetreatingManeuver(direction);
            }
            case Activity.TARGET_OUT_OF_RANGE: {
                return GetOutOfRangeManeuver(direction);
            }
            case Activity.AI_STRESSED: {
                return GetStressedManeuver(direction);
            }
            default:
                Debug.LogError("Invalid Activity - Default maneuver selected!");
                return maneuvers[0][0]; // Easiest maneuver
        }
    }

    // Get a maneuver when target is closing based on
    // target direction.
    private (Maneuver Maneuver, int Distance, int Difficulty) GetClosingManeuver (Direction direction) {
        int index = Random.Range(0, (maneuversWhenTargetIsClosing[(int)direction].Length));
        return maneuversWhenTargetIsClosing[(int)direction][index];
    }

    // Get a maneuver when target is retreating based on
    // target direction.
    private (Maneuver Maneuver, int Distance, int Difficulty) GetRetreatingManeuver (Direction direction) {
        int index = Random.Range(0, (maneuversWhenTargetIsRetreating[(int)direction].Length));
        return maneuversWhenTargetIsRetreating[(int)direction][index];
    }

    // Get a maneuver when target is out-of-range based on
    // target direction.
    private (Maneuver Maneuver, int Distance, int Difficulty) GetOutOfRangeManeuver (Direction direction) {
        int index = Random.Range(0, (maneuversWhenTargetIsOutOfRange[(int)direction].Length));
        return maneuversWhenTargetIsOutOfRange[(int)direction][index];
    }

    // Get a maneuver when target is out-of-range based on
    // target direction.
    private (Maneuver Maneuver, int Distance, int Difficulty) GetStressedManeuver (Direction direction) {
        int index = Random.Range(0, (maneuversWhenStressed[(int)direction].Length));
        return maneuversWhenStressed[(int)direction][index];
    }
}
