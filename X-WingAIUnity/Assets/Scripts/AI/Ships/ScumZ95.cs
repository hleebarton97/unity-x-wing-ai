﻿/// ////////////////////////////////////////////////////////
// Z 9 5 - A F 4   H E A D H U N T E R   ( S C U M )
/// ////////////////////////////////////////////////////////

public class ScumZ95 : Ship {

    // Used as constructor
    protected override void Start() {
        base.Start();

        // Maneuver dial
        // (Maneuver, Distance, Difficulty)
        this.maneuvers = new (Maneuver, int, int)[3][] {
            new (Maneuver, int, int)[] { // Blue
                (Maneuver.FORWARD, 1, 0),      // 0, 0
                (Maneuver.FORWARD, 2, 0),      // 0, 1
                (Maneuver.BANK_LEFT, 2, 0),    // 0, 2
                (Maneuver.BANK_RIGHT, 2, 0),   // 0, 3
                (Maneuver.FORWARD, 3, 0)       // 0, 4
            },
            new (Maneuver, int, int)[] { // White
                (Maneuver.BANK_LEFT, 1, 1),    // 1, 0
                (Maneuver.BANK_RIGHT, 1, 1),   // 1, 1
                (Maneuver.TURN_LEFT, 2, 1),    // 1, 2
                (Maneuver.TURN_RIGHT, 2, 1),   // 1, 3
                (Maneuver.TURN_LEFT, 3, 1),    // 1, 4
                (Maneuver.TURN_RIGHT, 3, 1),   // 1, 5
                (Maneuver.BANK_LEFT, 3, 1),    // 1, 6
                (Maneuver.BANK_RIGHT, 3, 1),   // 1, 7
                (Maneuver.FORWARD, 4, 1)       // 1, 8
            },
            new (Maneuver, int, int)[] { // Red
                (Maneuver.K_TURN, 3, 2),   // 2, 0
                (Maneuver.K_TURN, 4, 2)    // 2, 1
            }
        };

        /// ////////////////////////////////////////////////////////
        // T A R G E T   C L O S I N G
        /// ////////////////////////////////////////////////////////

        // Target is in range and facing AI
        this.maneuversWhenTargetIsClosing = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[1][0], // 10% chance of BANK_LEFT:1
                maneuvers[2][0],
                maneuvers[2][0], // 20% chance of K_TURN:3
                maneuvers[0][1],
                maneuvers[0][1],
                maneuvers[0][1], // 30% chance of FORWARD:2
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0]  // 40% chance of FORWARD:1
            },

            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0], // 10% chance of FORWARD:1
                maneuvers[1][3],
                maneuvers[1][3], // 20% chance of TURN_RIGHT:2
                maneuvers[0][3],
                maneuvers[0][3],
                maneuvers[0][3], // 30% chance of BANK_RIGHT:2
                maneuvers[1][1],
                maneuvers[1][1],
                maneuvers[1][1],
                maneuvers[1][1]  // 40% chance of BANK_RIGHT:1
                
            },

            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[2][0],
                maneuvers[2][0], // 20% chance of K_TURN:3
                maneuvers[2][1],
                maneuvers[2][1], // 20% chance of K_TURN:4
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3], // 30% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5], // 30% chance of TURN_RIGHT:3
            },

            // SOUTH EAST
            new (Maneuver, int, int)[10] {
                maneuvers[2][0], // 10% chance of K_TURN:3
                maneuvers[2][1], // 10% chance of K_TURN:4
                maneuvers[1][1], // 10% chance of BANK_RIGHT:1
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3], // 40% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5], // 30% chance of TURN_RIGHT:3
            },

            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[1][8], // 10% chance of FORWARD:4
                maneuvers[1][5], // 10% chance of TURN_RIGHT:3
                maneuvers[1][4], // 10% chance of TURN_LEFT:3
                maneuvers[2][0],
                maneuvers[2][0],
                maneuvers[2][0], // 30% chance of K_TURN:3
                maneuvers[2][1],
                maneuvers[2][1],
                maneuvers[2][1],
                maneuvers[2][1]  // 40% chance of K_TURN:4
            },

            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[1][0], // 10% chance of BANK_LEFT:1
                maneuvers[2][0], // 10% chance of K_TURN:3
                maneuvers[2][1], // 10% chance of K_TURN:4
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4], // 30% chance of TURN_LEFT:3
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2] // 40% chance of TURN_LEFT:2
            },

            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[2][0], // 10% chance of K_TURN:3
                maneuvers[2][1], // 10% chance of K_TURN:4
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 40% chance of TURN_LEFT:2
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4]  // 30% chance of TURN_LEFT:3
            },

            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0], // 10% chance of FORWARD:1
                maneuvers[1][2],
                maneuvers[1][2], // 20% chance of TURN_LEFT:2
                maneuvers[0][2],
                maneuvers[0][2],
                maneuvers[0][2], // 30% chance of BANK_LEFT:2
                maneuvers[1][0],
                maneuvers[1][0],
                maneuvers[1][0],
                maneuvers[1][0]  // 40% chance of BANK_LEFT:1
            },
        };

        /// ////////////////////////////////////////////////////////
        // T A R G E T   R E T R E A T I N G
        /// ////////////////////////////////////////////////////////

        // Target is in range and facing away from AI
        this.maneuversWhenTargetIsRetreating = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][4],
                maneuvers[0][4],
                maneuvers[0][4],
                maneuvers[0][4],
                maneuvers[0][4], // 50% chance of FORWARD:3
                maneuvers[1][8],
                maneuvers[1][8],
                maneuvers[1][8],
                maneuvers[1][8],
                maneuvers[1][8]  // 50% chance of FORWARD:4
            },

            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[1][3], // 10% chance of TURN_RIGHT:2
                maneuvers[1][5], // 10% chance of TURN_RIGHT:3
                maneuvers[0][3],
                maneuvers[0][3],
                maneuvers[0][3],
                maneuvers[0][3], // 40% chance of BANK_RIGHT:2
                maneuvers[1][7],
                maneuvers[1][7],
                maneuvers[1][7],
                maneuvers[1][7]  // 40% chance of BANK_RIGHT:3
            },

            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][3], // 10% chance of BANK_RIGHT:2
                maneuvers[1][7],
                maneuvers[1][7], // 20% chance of BANK_RIGHT:3
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3], // 30% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5]  // 40% chance of TURN_RIGHT:3
            },

            // SOUTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[2][0], // 10% chance of K_TURN:3
                maneuvers[2][1],
                maneuvers[2][1], // 20% chance of K_TURN:4
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3], // 30% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5]  // 40% chance of TURN_RIGHT:3
            },

            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[1][4], // 10% chance of TURN_LEFT:3
                maneuvers[1][5],
                maneuvers[1][5], // 20% chance of TURN_RIGHT:3
                maneuvers[2][0],
                maneuvers[2][0],
                maneuvers[2][0], // 30% chance of K_TURN:3
                maneuvers[2][1],
                maneuvers[2][1],
                maneuvers[2][1],
                maneuvers[2][1]  // 40% chance of K_TURN:4
            },

            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[2][0], // 10% chance of K_TURN:3
                maneuvers[2][1],
                maneuvers[2][1], // 20% chance of K_TURN:4
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 30% chance of TURN_LEFT:2
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4]  // 40% chance of TURN_LEFT:3
            },

            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][2], // 10% chance of BANK_LEFT:2
                maneuvers[1][6],
                maneuvers[1][6], // 20% chance of BANK_LEFT:3
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 30% chance of TURN_LEFT:2
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4]  // 40% chance of TURN_LEFT:3
            },

            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[1][2], // 10% chance of TURN_LEFT:2
                maneuvers[1][4], // 10% chance of TURN_LEFT:3
                maneuvers[0][2],
                maneuvers[0][2],
                maneuvers[0][2],
                maneuvers[0][2], // 40% chance of BANK_LEFT:2
                maneuvers[1][6],
                maneuvers[1][6],
                maneuvers[1][6],
                maneuvers[1][6]  // 40% chance of BANK_LEFT:3
            }
        };

        /// ////////////////////////////////////////////////////////
        // T A R G E T   O U T   O F   R A N G E
        /// ////////////////////////////////////////////////////////

        // Target is not in range
        this.maneuversWhenTargetIsOutOfRange = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[1][6], // 10% chance of BANK_LEFT:3
                maneuvers[1][7], // 10% chance of BANK_RIGHT:3
                maneuvers[0][4],
                maneuvers[0][4],
                maneuvers[0][4],
                maneuvers[0][4], // 40% chance of FORWARD:3
                maneuvers[1][8],
                maneuvers[1][8],
                maneuvers[1][8],
                maneuvers[1][8]  // 40% chance of FORWARD:4
            },

            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[1][3], // 10% chance of TURN_RIGHT:2
                maneuvers[1][5], // 10% chance of TURN_RIGHT:3
                maneuvers[0][3],
                maneuvers[0][3],
                maneuvers[0][3],
                maneuvers[0][3], // 40% chance of BANK_RIGHT:2
                maneuvers[1][7],
                maneuvers[1][7],
                maneuvers[1][7],
                maneuvers[1][7], // 40% chance of BANK_RIGHT:3

            },

            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 50% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5], // 50% chance of TURN_RIGHT:3
            },

            // SOUTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 50% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5]  // 50% chance of TURN_RIGHT:3
            },

            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[1][2],
                maneuvers[1][2], // 20% chance of TURN_LEFT:2
                maneuvers[1][4],
                maneuvers[1][4], // 20% chance of TURN_LEFT:3
                maneuvers[1][3],
                maneuvers[1][3], // 20% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5], // 20% chance of TURN_RIGHT:3
                maneuvers[2][1],
                maneuvers[2][1]  // 20% chance of K_TURN:4
            },

            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 50% chance of TURN_LEFT:2
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4]  // 50% chance of TURN_LEFT:3
            },

            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 50% chance of TURN_LEFT:2
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4]  // 50% chance of TURN_LEFT:3
            },

            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[1][2], // 10% chance of TURN_LEFT:2
                maneuvers[1][4], // 10% chance of TURN_LEFT:3
                maneuvers[0][2],
                maneuvers[0][2],
                maneuvers[0][2],
                maneuvers[0][2], // 40% chance of BANK_LEFT:2
                maneuvers[1][6],
                maneuvers[1][6],
                maneuvers[1][6],
                maneuvers[1][6]  // 40% chance of BANK_LEFT:3
            }
        };

        /// ////////////////////////////////////////////////////////
        // A I   S T R E S S E D
        /// ////////////////////////////////////////////////////////

        // AI ship is stressed
        this.maneuversWhenStressed = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[1][0], // 10% chance of BANK_LEFT:1
                maneuvers[0][2], // 10% chance of BANK_LEFT:2
                maneuvers[0][3], // 10% chance of BANK_RIGHT:2
                maneuvers[1][8], // 10% chance of FORWARD:4
                maneuvers[0][0],
                maneuvers[0][0], // 20% chance of FORWARD:1
                maneuvers[0][1],
                maneuvers[0][1], // 20% chance of FORWARD:2
                maneuvers[0][4],
                maneuvers[0][4], // 20% chance of FORWARD:3
            },

            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0], // 10% chance of FORWARD:1
                maneuvers[1][3], // 10% chance of TURN_RIGHT:2
                maneuvers[1][5], // 10% chance of TURN_RIGHT:3
                maneuvers[0][3],
                maneuvers[0][3], // 20% chance of BANK_RIGHT:2
                maneuvers[1][7],
                maneuvers[1][7], // 20% chance of BANK_RIGHT:3
                maneuvers[1][1],
                maneuvers[1][1],
                maneuvers[1][1]  // 30% chance of BANK_RIGHT:1
            },

            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3], // 50% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5]  // 50% chance of TURN_RIGHT:3
            },

            // SOUTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3],
                maneuvers[1][3], // 50% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5],
                maneuvers[1][5]  // 50% chance of TURN_RIGHT:3
            },

            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[1][3],
                maneuvers[1][3], // 20% chance of TURN_RIGHT:2
                maneuvers[1][5],
                maneuvers[1][5], // 20% chance of TURN_RIGHT:3
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 30% chance of TURN_LEFT:2
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4], // 30% chance of TURN_LEFT:3
            },

            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 50% chance of TURN_LEFT:2
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4], // 50% chance of TURN_LEFT:3
            },

            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2],
                maneuvers[1][2], // 50% chance of TURN_LEFT:2
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4],
                maneuvers[1][4], // 50% chance of TURN_LEFT:3
            },

            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0], // 10% chance of FORWARD:1
                maneuvers[1][2], // 10% chance of FORWARD:1
                maneuvers[1][4], // 10% chance of FORWARD:1
                maneuvers[0][2],
                maneuvers[0][2], // 20% chance of BANK_LEFT:2
                maneuvers[1][6],
                maneuvers[1][6], // 20% chance of BANK_LEFT:3
                maneuvers[1][0],
                maneuvers[1][0],
                maneuvers[1][0]  // 30% chance of BANK_LEFT:1
            }
        };
    }
}
