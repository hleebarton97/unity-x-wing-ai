﻿/// ////////////////////////////////////////////////////////
// S H I P   N A M E   ( F A C T I O N )
/// ////////////////////////////////////////////////////////

public class FactionShipTemplate : Ship {
    // Used as constructor
    protected override void Start() {
        base.Start();

        // Maneuver dial
        maneuvers = new (Maneuver, int, int)[3][] {
            new (Maneuver, int, int)[] { // Blue
            },
            new (Maneuver, int, int)[] { // White
            },
            new (Maneuver, int, int)[] { // Red
            },
        };

        /// ////////////////////////////////////////////////////////
        // T A R G E T   C L O S I N G
        /// ////////////////////////////////////////////////////////

        // Target in range and facing AI
        this.maneuversWhenTargetIsClosing = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
        };

        /// ////////////////////////////////////////////////////////
        // T A R G E T   R E T R E A T I N G
        /// ////////////////////////////////////////////////////////

        // Target is in range and facing away from AI
        this.maneuversWhenTargetIsRetreating = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
        };

        /// ////////////////////////////////////////////////////////
        // T A R G E T   O U T   O F   R A N G E
        /// ////////////////////////////////////////////////////////

        // Target is not in range
        this.maneuversWhenTargetIsOutOfRange = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
        };

        /// ////////////////////////////////////////////////////////
        // A I   S T R E S S E D
        /// ////////////////////////////////////////////////////////

        // AI ship is stressed
        this.maneuversWhenStressed = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
        };
    }
}

